import React from "react";
import { NavLink } from "react-router-dom";

/*<span className="brand-logo" style={{ padding: "0 8px" }}>
          <NavLink to="/">Celus</NavLink>
        </span>*/

export const Navbar = () => {
  return (
    <nav>
      <div className="nav-wrapper black darken-1" style={{ padding: "0 2rem" }}>
        <img
          src="/celus.jpeg"
          alt="Celus"
          style={{ height: "50px", paddingTop: "10px" }}
        />
        <ul id="nav-mobile" className="right hide-on-med-and-down">
          <li>
            <NavLink to="/query">Send a query</NavLink>
          </li>
          <li>
            <NavLink to="/file">Manage files</NavLink>
          </li>
        </ul>
      </div>
    </nav>
  );
};
