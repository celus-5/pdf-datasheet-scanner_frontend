import React, { useEffect } from "react";
import Highlighter from "react-highlight-words";

export const DisplayFound = (props) => {
  useEffect(() => {
    window.M.updateTextFields();
  }, []);

  const generateTable = (table) => {
    table = JSON.parse(table);
    console.log(table);
    return (
      <table>
        <thead>
          <tr>
            {table.schema.fields.map((col) => (
              <th>
                <Highlighter
                  searchWords={[props.query]}
                  autoEscape={true}
                  textToHighlight={col.name.toString()}
                />
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {table.data.map((row) => (
            <tr>
              {table.schema.fields.map((col) => (
                <td>
                  <Highlighter
                    searchWords={[props.query]}
                    autoEscape={true}
                    textToHighlight={row[col.name.toString()].toString()}
                  />
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
    );
  };

  return (
    <div className="row">
      <div className="col s8 offset-s2">
        <div className="card white darken-1">
          <div className="card-content black-text">
            <span className="card-title">Found Text</span>
            <div>
              <ul>
                {props.found.text.map((line) => (
                  <li>
                    <Highlighter
                      searchWords={[props.query]}
                      autoEscape={true}
                      textToHighlight={line}
                    />
                  </li>
                ))}
              </ul>
            </div>
            <div>{props.found.tables.map((t) => generateTable(t))}</div>
          </div>
        </div>
      </div>
    </div>
  );
};
