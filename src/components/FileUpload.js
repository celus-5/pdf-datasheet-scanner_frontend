import React, { useState } from "react";
import { useMessage } from "../hooks/message.hook";
import axios from "axios";
import { loadProgressBar } from "axios-progress-bar";

export const FileUpload = () => {
  const message = useMessage();
  const [file, setFile] = useState("");
  const [uploaded, setUploaded] = useState(false);

  const changeHandler = (e) => {
    e.preventDefault();
    switch (e.target.name) {
      case "file":
        if (validateSize(e.target.files)) setFile(e.target.files[0]);
        break;
      default:
        setFile("");
        setUploaded(false);
    }
  };

  const formSubmitHandler = (e) => {
    e.preventDefault();

    if (!file) {
      message("Add a file.");
      return;
    }

    let formData = new FormData();
    formData.append("file", file);
    console.log(file);

    loadProgressBar();

    axios
      .post("/file/upload", formData)
      .then((res) => setUploaded(true))
      .catch((err) => message(err.message));
  };

  const validateSize = (files) => {
    let file = files[0];
    const size = 16 * 1024 * 1024;

    if (!file) {
      return false;
    }

    if (file.size > size) {
      message("This file is too large, please pick a smaller file.");
      return false;
    }
    return true;
  };

  return (
    <div className="row">
      <form onSubmit={formSubmitHandler} className="col s8 offset-s2">
        <div className="card white darken-1">
          {!uploaded && (
            <div className="card-content black-text">
              <span className="card-title">File</span>
              <div className="file-field input-field">
                <div className="btn blue waves-effect waves-light">
                  <span>Upload</span>
                  <input type="file" name="file" onChange={changeHandler} />
                </div>
                <div className="file-path-wrapper">
                  <input
                    disabled
                    name="file"
                    className="file-path"
                    type="text"
                    value={file}
                  />
                  <label htmlFor="file">Select a file</label>
                </div>
              </div>
            </div>
          )}
          {uploaded && (
            <div className="card-content black-text">
              <span className="card-title">
                File has been successfully uploaded.
              </span>
            </div>
          )}
          <div className="card-action">
            <button type="submit" className="btn blue waves-effect waves-light">
              Submit
              <i className="material-icons right">send</i>
            </button>
            {!uploaded && (
              <button
                type="delete"
                name="delete"
                className="btn white red-text waves-effect waves-dark"
                onClick={changeHandler}
              >
                x
              </button>
            )}
            {uploaded && (
              <button
                type="replay"
                name="replay"
                className="btn white blue-text waves-effect waves-dark"
                onClick={changeHandler}
              >
                <i className="material-icons">replay</i>
              </button>
            )}
          </div>
        </div>
      </form>
    </div>
  );
};
