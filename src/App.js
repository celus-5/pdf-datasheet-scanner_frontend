import React from "react";
import { useRoutes } from "./routes.js";
import { BrowserRouter as Router } from "react-router-dom";
import { Navbar } from "./components/Navbar";
import { Loader } from "./components/Loader";
import "materialize-css";

function App() {
  const isAuthenticated = true;
  const ready = true;
  const routes = useRoutes(isAuthenticated);

  if (!ready) {
    return <Loader />;
  }

  return (
    <div className="App">
      <Router>
        <Navbar />
        <div className="container">{routes}</div>
      </Router>
      <link
        rel="stylesheet"
        href="https://fonts.googleapis.com/icon?family=Material+Icons"
      />
    </div>
  );
}

export default App;
