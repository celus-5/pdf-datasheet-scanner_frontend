import React, { useState } from "react";
import { useMessage } from "../hooks/message.hook";
import axios from "axios";
import { loadProgressBar } from "axios-progress-bar";
import { DisplayFound } from "../components/DisplayFound";

export const QueryPage = () => {
  const message = useMessage();
  const [form, setForm] = useState({
    component: "",
    query: "",
  });

  const [found, setFound] = useState({
    text: [],
    tables: [],
  });

  const changeHandler = (e) => {
    switch (e.target.name) {
      case "replay":
        setForm({
          component: "",
          query: "",
        });
        setFound({
          text: [],
          tables: [],
        });
        break;
      default:
        setForm({ ...form, [e.target.name]: e.target.value });
    }
  };

  const formSubmitHandler = async (e) => {
    e.preventDefault();

    if (!form.component || !form.query) {
      message("All fileds must be filled up.");
      return;
    }

    loadProgressBar();

    axios
      .post("/query", { ...form })
      .then((res) => {
        if (res && res.data && (res.data.text || res.data.tables)) {
          setFound({ text: res.data.text, tables: res.data.tables });
        } else {
          message("Invalid server response.");
        }
      })
      .catch((err) => message(err.message));
  };

  return (
    <div className="row">
      <form onSubmit={formSubmitHandler} className="col s8 offset-s2">
        <div className="card white darken-1">
          <div className="card-content black-text">
            <span className="card-title">Query</span>
            <div className="input-field">
              <input
                id="component"
                type="text"
                name="component"
                value={form.component}
                onChange={changeHandler}
                className="blue-input"
              />
              <label htmlFor="component">Component</label>
            </div>
            <div className="input-field">
              <input
                id="query"
                type="text"
                name="query"
                value={form.query}
                onChange={changeHandler}
                className="blue-input"
              />
              <label htmlFor="query">What should be found</label>
            </div>
          </div>
          <div className="card-action">
            <div className="input-field">
              <button
                type="submit"
                className="btn blue waves-effect waves-light"
              >
                Submit
                <i className="material-icons right">send</i>
              </button>
              {(!!found.text.length || !!found.tables.length) && (
                <button
                  type="replay"
                  name="replay"
                  className="btn white blue-text waves-effect waves-dark"
                  onClick={changeHandler}
                >
                  <i className="material-icons">replay</i>
                </button>
              )}
            </div>
          </div>
        </div>
      </form>
      {(!!found.text.length || !!found.tables.length) && (
        <div>
          <DisplayFound found={found} query={form.query} />
        </div>
      )}
    </div>
  );
};
