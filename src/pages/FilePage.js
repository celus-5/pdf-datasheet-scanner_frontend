import React from "react";
import { useMessage } from "../hooks/message.hook";
import { FileUpload } from "../components/FileUpload";

export const FilePage = () => {
  const message = useMessage();

  return (
    <div>
      <FileUpload />
    </div>
  );
};

