import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { QueryPage } from './pages/QueryPage';
import { FilePage } from './pages/FilePage';
//import { AuthPage } from './pages/AuthPage.js';

export const useRoutes = isAuthenticated => {
	if (isAuthenticated) {
		return (
			<Switch>
				<Route path="/query" exact>
					<QueryPage />
				</Route>
				<Route path="/file" exact>
					<FilePage />
				</Route>
				<Redirect to="/query"/>
			</Switch>
		);
	}

	return (
		<Switch>
			<Route path="/" exact>
				<QueryPage />
			</Route>
			<Redirect to="/" />
		</Switch>
	);
};
